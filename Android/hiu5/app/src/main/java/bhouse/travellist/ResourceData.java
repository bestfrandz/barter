package bhouse.travellist;

import java.util.ArrayList;

/**
 * Created by megha on 15-03-06.
 */
public class ResourceData {

    public static String[] resourceNameArray = {"Calculator", "Notes", "Gloves", "Tutoring"};

    public static ArrayList<Resource> resourceList() {
        ArrayList<Resource> list = new ArrayList<>();
        for (int i = 0; i < resourceNameArray.length; i++) {
            Resource resource = new Resource();
            resource.name = resourceNameArray[i];
            resource.imageName = resourceNameArray[i].replaceAll("\\s+", "").toLowerCase();
            if (i == 2 || i == 5) {
                resource.isFav = true;
            }
            list.add(resource);
        }
        return (list);
    }

    public static Resource getItem(String _id) {
        for (Resource resource : resourceList()) {
            if (resource.id.equals(_id)) {
                return resource;
            }
        }
        return null;
    }
}
