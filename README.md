# Barter #
By: Ammar Husain, Swetha Revanaur, and Tejas Manjunath

Barter is a project created to allow students to exchange resources.

### Story ###

Barter was an idea that was formed out of the frustration of three High School Students. In school we are instructed to use a service called Schoolloop to communicate with each other and receive updates about our classes. Teachers post their assignments and grades, and students can email each other for help. 
However this never happens. While Schoolloop is an amazing service, it lacks a key element. To fill this void, students instead flock to Facebook in search of a network in which they can truly collaborate, share, and learn in a more social environment. This however comes with its own issues. Facebook is an uncontrolled environment designed for causal socialization, not necessarily an appropriate space for learning. Cheating runs rampant, and with a feed full of funny cat videos and your friend's new profile picture, productivity is thrown out the door.

### Repo Structure ###

The Repo consists of two main folders, one for the Android application and the other for the server. 

+ Server
    * handlers.py: All the GET/POST request for the file
    * main.py: webapp2 config, routes to handler functions
    * datamanage.py: Data Schema
+ Application


### Links ###

* Demo Video: https://drive.google.com/file/d/0B6CEE9NynzFnYmR5ZGVqNE02SGc/view?usp=sharing