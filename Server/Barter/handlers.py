# -*- coding: utf-8 -*-
import logging
import os
import webapp2
import webob.multidict
import jinja2
import string
from data_manage import Trades
from data_manage import Users
from data_manage import Communities
from data_manage import Resources
import urllib
import random
from webapp2_extras import auth, sessions
from jinja2.runtime import TemplateNotFound

from google.appengine.ext.webapp import template


from google.appengine.ext import ndb
import json

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class AuthLogin(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))
    res = {
      'loginStatus': 'Success',
      'message': 'User ' + req['email'] + ' has logged in.'
    }
    self.response.write(json.dumps(res))

class DashFeed(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))
    feed = []
    if req['feedLength'] <= 20:
      resources = Resources.query().fetch(20)
      for resource in resources:
        ob = {
          'title' : resource.title,
          'description' : resource.description,
          'creator' : resource.creator,
          'rtype' : resource.rtype,
          'resourceid' : resource.resourceid,
          'communities' : resource.communities,
          'tags' : resource.tags,
          'trades' :  resource.trades
        }
        feed.append(ob)
    res = {
      'feed': feed
      }        

    self.response.write(json.dumps(res))

class CommunityFeed(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))
    feed = []
    if req['feedLength'] <= 20:
      resources = Resources.query(Resources.communities == req['communityName']).fetch(req['feedLength'])
      for resource in resources:
        ob = {
          'title' : resource.title,
          'description' : resource.description,
          'creator' : resource.creator,
          'rtype' : resource.rtype,
          'resourceid' : resource.resourceid,
          'communities' : resource.communities,
          'tags' : resource.tags,
          'trades' :  resource.trades
        }
        feed.append(ob)
    res = {
      'communityName':  req['communityName'],
      'feed': feed
      }
    self.response.write(json.dumps(res))

class TradeFeed(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))

    feed = []
    if req['feedLength'] <= 20:
      trades = Trades.query(Trades.items2 == req['resourceid']).fetch(req['feedLength'])
      for trade in trades:
        ob = {
          'tradeid': trade.tradeid,
          'email1': trade.email1,
          'email2': trade.email2,
          'description': trade.description,
          'status': trade.status,
          'items1': trade.items1,
          'items2': trade.items2
        }
        feed.append(ob)
    res = {
      'resourceid':  req['resourceid'],
      'feed': feed
      }        

    self.response.write(json.dumps(res))

class ProfileTrades(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))

    feed = []
    if req['feedLength'] <= 20:
      trades = Trades.query(Trades.email1 == req['email']).fetch(req['feedLength'])
      for trade in trades:
        ob = {
          'tradeid': trade.tradeid,
          'email1': trade.email1,
          'email2': trade.email2,
          'description': trade.description,
          'status': trade.status,
          'items1': trade.items1,
          'items2': trade.items2
        }
        feed.append(ob)

    res = {
      'email':  req['email'],
      'tradefeed': feed
      }
    self.response.write(json.dumps(res))

class ProfileFeed(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))
    
    feed = []
    if req['feedLength'] <= 20:
      resources = Resources.query(Resources.creator == req['email']).fetch(req['feedLength'])
      for resource in resources:
        ob = {
          'title' : resource.title,
          'description' : resource.description,
          'creator' : resource.creator,
          'rtype' : resource.rtype,
          'resourceid' : resource.resourceid,
          'communities' : resource.communities,
          'tags' : resource.tags,
          'trades' :  resource.trades
        }
        feed.append(ob)
    res = {
      'email':  req['email'],
      'feed': feed
      }         
    self.response.write(json.dumps(res))

class ResourceData(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))
    resource = Resources.query(Resources.resourceid == req['resourceid']).fetch(1)[0]
    res = {
      'title': resource.title,
      'description': resource.description,
      'rtype': resource.rtype,
      'resourceid': resource.resourceid,
      'communities': resource.communities,
      'tags': resource.tags
    }
    self.response.write(json.dumps(res))

class CreateResource(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))
    rid= Resources.allocate_ids(size=1)[0]
    rando = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
    rcid= rando + "-" + str(rid)
    tKey = ndb.Key('Resources', rid)
    resource = Resources(key=tKey, resourceid = rcid, title= req['title'], description= req['description'], rtype = req['resourceType'], communities = req['communities'], tags = req['tags'], creator = req['email'])
    resource.put()
    res = {
      'message': 'Resource created'
    }
    self.response.write(json.dumps(res))



    
class CreateTrade(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))

    tid= Trades.allocate_ids(size=1)[0]
    rando = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
    tcid= rando + "-" + str(tid)
    tKey = ndb.Key('Trades', tid)
    trade = Trades(key=tKey, tradeid = tcid, email1= req['email1'], email2= req['email2'], items1= req['items1'], items2= req['items2'], description= req['description'], status = 'Pending')
    trade.put()
    res = {
      'message': 'Trade created'
    }
    self.response.write(json.dumps(res))

    

class UpdateTrade(webapp2.RequestHandler):
  def get(self):
    self.response.write('Use post method please.')
  def post(self):
    req = json.loads(urllib.unquote(self.request.body))

    trade = Trades.query(Trades.tradeid == req['tcid']).fetch(1)[0]
    if trade.status == 'Pending' and (req['status'] == 'Approved' or req['status'] == 'Declined'):
      trade.status = req['status']
      trade.put()

    res = {
      'loginStatus': 'Success',
      'message': 'Feed for ' + req['email']
    }
    self.response.write(json.dumps(res))
    


    



'''
class RootHandler(webapp2.RequestHandler):
  def get(self):
    if self.logged_in:
      self.redirect('/fp/profile')
    else:
      self.redirect('/fp/login')
class Login(BaseRequestHandler):
  def get(self):
    if self.logged_in:
      self.redirect('/fp/profile')
    else:
      template = JINJA_ENVIRONMENT.get_template('login.html')
      self.response.write(template.render())
    

class ProfileHandler(BaseRequestHandler):
  def get(self):
    """Handles GET /profile"""
    if self.logged_in:
      template_values={ 
        'name': self.current_user.name,
        'user_img': self.current_user.avatar_url     
      }
      template = JINJA_ENVIRONMENT.get_template('profile.html')
      self.response.write(template.render(template_values))
    else:
      self.redirect('/fp/login')

#Sensor Page Handlers
class GetUserSensors(BaseRequestHandler):
  def get(self):
    if self.logged_in:
      self.response.headers['Content-Type'] = 'application/json'   
      jdata = {
        'data': []
      } 
      usr = self.current_user
      i = 0
      for snsKey in usr.Sensors:
        csnsr = FluxSensors.get(snsKey)
        obj['data'][i]={
        'Name':csnsr.name,
        'ID':csnsr.ConsumId,
        'Location':csnsr.location,
        'Sublocation':csnsr.sublocation
        }
        i += 1
      self.response.out.write(json.dumps(jdata))
    else:
      self.redirect('/fp/login')

class SensorForm(BaseRequestHandler):
  def get(self):
    if self.logged_in:
      self.response.headers['Content-Type'] = 'application/json'  
      usr = self.current_user
      jdata = {
      'locations':[]
      } 
      i = 0
      for location in usr.locations:
        jdata['locations'].append(location)
        i += 1
      self.response.out.write(json.dumps(jdata))
    else:
      self.redirect('/fp/login')
  def post(self):
      logging.debug(self.request.body)
      b = json.loads(self.request.body)
      self.current_user.Sensors.append(b['id']).put()
      self.response.out.write("Success")
    
#Handle User Data
class UserData(BaseRequestHandler):
  def get(self):
    if self.logged_in:
      self.response.headers['Content-Type'] = 'application/json'
      obj = {
        'Sensors':[]
      }
      a = 0;
      for item in self.current_user.Sensors:
        snsr = FluxSensors.query(FluxSensors.ConsumId == item).fetch()[0]
        sessions = FluxSessions.query(ancestor=snsr.key).fetch()
        obj1= {
        'name':snsr.name,
        'id': snsr.ConsumId,
        'location' : snsr.location,
        'sublocation' : snsr.sublocation,
        'sessions': []
        }
        b = 0;
        for sess in sessions:
          sss = {
            'Time':sess.DateTime,
            'AverageTemperature':sess.AverageTemp,
            'mlUsed':sess.mlUsed
          }
          obj1['sessions'].append(sss)
        obj['Sensors'].append(obj1)
      self.response.out.write(json.dumps(obj))

    else:
      self.redirect('/fp/login')
    

class DataSubmit(webapp2.RequestHandler):
  def post(self):
    b = json.loads(self.request.body)
    temp=0
    mlUsed=0
    if b['datatype'] == 'JSON':
      runningTotal=0
      for i in xrange(len(b['rawData'])):
        runningTotal+= float(b['rawData'][i]['temperature'])
      temp= float(runningTotal/(len(b['rawData'])))
      mlUsed= int(b['rawData'][len(b['rawData'])-1]['mlUsed'])
    else:
      mlUsed= b['aggData']['mlUsed']
      temp= b['aggData']['avgTemperature']
    state = _NewData(b['uniqueId'], temp, mlUsed)
    status=""
    if state:
      status="Success"
    else:
      status="Failure"
    self.response.write(status)

    '''