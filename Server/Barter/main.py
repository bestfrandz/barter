import sys

from webapp2 import WSGIApplication, Route
from data_manage import Users
import handlers

# inject './lib' dir in the path so that we can simply do "import ndb"
# or whatever there's in the app lib dir.
if 'lib' not in sys.path:
  sys.path[0:0] = ['lib']

# webapp2 config



# Map URLs to handlers
routes = [
  Route('/auth/login', handlers.AuthLogin),
  Route('/views/dash', handlers.DashFeed),
  Route('/views/community', handlers.CommunityFeed),
  Route('/views/trade', handlers.TradeFeed),
  Route('/views/profile', handlers.ProfileFeed),
  Route('/resource/data', handlers.ResourceData),
  Route('/resource/create', handlers.CreateResource),
  Route('/trade/create', handlers.CreateTrade),
  Route('/trade/update', handlers.UpdateTrade),
  Route('/profile/trades', handlers.ProfileTrades)
]

webapp = WSGIApplication(routes, debug=True)

