from google.appengine.ext import ndb
import webapp2_extras.appengine.auth.models as auth_models

#class FLuxUser(ndb.Model):
  # Sensor Entity Group
#Users
class Users(auth_models.User):
  firstname = ndb.StringProperty();
  lastname = ndb.StringProperty();  
  email= ndb.StringProperty()
  school =ndb.StringProperty()
  communities = ndb.StringProperty(repeated=True)
  resources= ndb.StringProperty(repeated=True)
  sublocations= ndb.StringProperty(repeated=True)
#Resources
class Resources(ndb.Model):
  title= ndb.StringProperty()
  description= ndb.StringProperty()
  creator= ndb.StringProperty()
  rtype=ndb.StringProperty()
  resourceid= ndb.StringProperty()
  communities= ndb.StringProperty(repeated=True)
  tags= ndb.StringProperty(repeated=True)
  trades = ndb.StringProperty(repeated = True)
# Communitie
class Communities(ndb.Model):
  name = ndb.StringProperty()
  description = ndb.StringProperty()
  ctype= ndb.StringProperty()
  usercount= ndb.IntegerProperty()
#Trade
class Trades(ndb.Model):
  tradeid = ndb.StringProperty()
  email1 = ndb.StringProperty()
  email2 = ndb.StringProperty()
  description= ndb.StringProperty()
  status= ndb.StringProperty()
  items1= ndb.StringProperty(repeated = True)
  items2= ndb.StringProperty(repeated = True)
